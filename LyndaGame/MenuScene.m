//
//  MenuScene.m
//  LyndaGame
//
//  Created by Dan Lakss on 2015-03-02.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//


#import "MenuScene.h"
#import "GameScene.h"


@implementation MenuScene

//didMoveToView - Entry point
-(void)didMoveToView:(SKView *)view{
    NSLog(@"MenuScene entered!");
    SKLabelNode *label = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    label.position = CGPointMake(500, 500);
    label.fontColor=[SKColor blueColor];
    label.fontSize=50;
    label.text = @"Game over, man! Game over!";
    self.backgroundColor=[UIColor clearColor];
    [self addChild:label];
    
    //Second label
    SKLabelNode *tryAgainLabel = [SKLabelNode labelNodeWithFontNamed:@"Courier"];
    tryAgainLabel.text = @"Tap to try again.";
    tryAgainLabel.position = CGPointMake(CGRectGetMidX(self.frame), CGRectGetMidY(self.frame));
    tryAgainLabel.fontColor = [SKColor blueColor];
    tryAgainLabel.fontSize= 40;
    [self addChild:tryAgainLabel];
}

//touchesBegan - start new scene from here
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    GameScene *gameScene = [GameScene sceneWithSize:self.frame.size];
    [self.view presentScene:gameScene];
}
@end
