//
//  GameScene.m
//  LyndaGame
//
//  Created by Dan Lakss on 2015-02-23.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "GameScene.h"
#import "MenuScene.h"

@interface GameScene()
@property (nonatomic) Player *player;
@property (nonatomic) SKSpriteNode *ball;
@property (nonatomic) CGVector ballSpeed;
@property (nonatomic) BOOL isDead; //Used to transition to game over scene
@property (nonatomic) int score;
@end

//Physics categories - used to check bodies against each other
static const u_int32_t topEdgeCategory = 1; //Boundary category used for physics
static const u_int32_t ballCategory = 2;

@implementation GameScene

//write code for contacts here
- (void)didBeginContact:(SKPhysicsContact *)contact{
    NSLog(@"Contact!");
    if(contact.bodyA.categoryBitMask == topEdgeCategory){
        NSLog(@"top edge touced");
        self.score++;
        NSLog(@"Score: %i", self.score);
        [self.ball.physicsBody applyImpulse:CGVectorMake(0, self.ball.physicsBody.velocity.dy+0.1)];


    }
}

//didMoveToView - entry point in scene
-(void)didMoveToView:(SKView *)view {
    /* Setup your scene here */
    self.score = 0;
    
    //Set scene physics body
    self.physicsBody = [SKPhysicsBody bodyWithEdgeLoopFromRect:self.frame];
    
    //Init player sprite
    self.player = [[Player alloc]init];
    [self.player initialize:@"paddle" withPositionX:self.frame.size.width/2 withPositionY:200];
    [self addChild:self.player.sprite];
    self.player.sprite.physicsBody.friction = 0;
    
    //Init ball
    self.ball = [SKSpriteNode spriteNodeWithImageNamed:@"ball"];
    self.ball.position = CGPointMake(self.frame.size.width/2, self.frame.size.height/2 + 100);
    self.ball.physicsBody = [SKPhysicsBody bodyWithCircleOfRadius:self.ball.frame.size.width/2];
    self.ball.physicsBody.dynamic = true;
    NSLog(@"Ball physics dynamic: %hhd", self.ball.physicsBody.dynamic);
    [self addChild:self.ball];
    [self.ball.physicsBody applyImpulse:CGVectorMake(arc4random() % 50, 10)];
    self.ballSpeed = self.ball.physicsBody.velocity;
    self.ball.physicsBody.friction = 0.0;
    self.ball.physicsBody.density = 0.0;
    self.ball.physicsBody.restitution = 1;
    self.ball.physicsBody.linearDamping = 0;
    self.ball.physicsBody.categoryBitMask = ballCategory;
    self.ball.physicsBody.contactTestBitMask = topEdgeCategory;
    

    
    //World physics
    self.physicsWorld.gravity = CGVectorMake(0, 0);
    CGVector origin = CGVectorMake(self.frame.origin.x, self.frame.origin.y);
    CGVector size = CGVectorMake(self.frame
                             .size.width, 	self.frame.size.height);
    NSLog(@"Scene origin: X= %f Y=%f", origin.dx, origin.dy);
    NSLog(@"Scene size: X= %f Y=%f", size.dx, size.dy);
    self.physicsWorld.contactDelegate = self;
    [self setInvisibleWall];
    

}

//setInvisibleWall - sets boundary for score counting
-(void)setInvisibleWall{
    SKNode *node = [SKNode node];
    node.physicsBody = [SKPhysicsBody bodyWithEdgeFromPoint:CGPointMake(0, self.frame
                                                                        .size.height) toPoint:CGPointMake(self.frame.size.width, self.frame.size.height)];
    node.physicsBody.categoryBitMask = topEdgeCategory;

    [self addChild:node];
                                                                                                    
                                                                                                        
}

//checkGameState - checks if ball is above player
-(void)checkGameState{
    //Checks and sets game state (if player lost etc.)
    if(self.ball.position.y > self.player.sprite.position.y){
        self.isDead = false;
    }else{
        self.isDead = true;
    }
    
    if(self.isDead){
        MenuScene *menuScene = [MenuScene sceneWithSize:self.frame.size];
        [self.view presentScene:menuScene];
    }
        
}

//touchesMoved - updates player movement
-(void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{

    for(UITouch *touch in touches)
    {
        CGPoint position = [touch locationInNode:self];
        

        CGPoint newPosition = CGPointMake(position.x, self.player.sprite.position.y);
        if(self.player.isMoving){
            self.player.sprite.position = newPosition;
        }
        
        

    }
    
    
    
}

//touchesBegan - starts player movement
-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    for(UITouch *touch in touches)
    {
        

        //Check if touches are inside of player radius in x-axis
        CGPoint touchPosition = [touch locationInNode:self];
        if(touchPosition.x >= self.player.sprite.position.x - self.player.sprite.frame.size.width/2 && touchPosition.x <= self.player.sprite.position.x + self.player.sprite.frame.size.width/2){
            self.player.isMoving = YES;
            self.player.sprite.position = CGPointMake(touchPosition.x, self.player.sprite.position.y);
        }
        
        
        
        
        
    }
}

//touchesEnded - releases player movement
-(void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event{
    self.player.isMoving = false;
}




//update - checks game state I.E. is player alive?
-(void)update:(CFTimeInterval)currentTime {
    /* Called before each frame is rendered */
//    NSLog(@"Distance: %f",[self.player calcDistanceToObject:self.ball.position.x
//                                            withObjectPosY:self.ball.position.y]);
    [self checkGameState];

}

@end
