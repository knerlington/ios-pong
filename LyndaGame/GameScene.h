//
//  GameScene.h
//  LyndaGame
//

//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>
#import "Player.h"


@interface GameScene : SKScene <SKPhysicsContactDelegate>

@end
