//
//  AppDelegate.h
//  LyndaGame
//
//  Created by Dan Lakss on 2015-02-23.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

