//
//  Player.m
//  LyndaGame
//
//  Created by Dan Lakss on 2015-02-23.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import "Player.h"

@implementation Player

//Set sprite, position etc.
-(void)initialize:(NSString *)imageName withPositionX:(float)x withPositionY:(float)y{
    self.sprite = [SKSpriteNode spriteNodeWithImageNamed:imageName];
    self.sprite.physicsBody = [SKPhysicsBody bodyWithRectangleOfSize:self.sprite.frame.size];
    self.sprite.position = CGPointMake(x, y);
    self.sprite.physicsBody.density = 1;
    self.sprite.physicsBody.dynamic = false;
}

//calcDistanceToObject()
-(float)calcDistanceToObject:(float)withObjectPosX withObjectPosY:(float)posY{
    float distanceX = (withObjectPosX - self.sprite.position.x) * (withObjectPosX - self.sprite.position.x);
    float distanceY = (posY - self.sprite.position.y) * (posY - self.sprite.position.y);
    float distance = sqrtf(distanceX+distanceY);
    return distance;
}
@end
