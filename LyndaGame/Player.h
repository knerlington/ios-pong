//
//  Player.h
//  LyndaGame
//
//  Created by Dan Lakss on 2015-02-23.
//  Copyright (c) 2015 Dan Lakss. All rights reserved.
//

#import <SpriteKit/SpriteKit.h>

@interface Player : NSObject


@property (nonatomic) SKSpriteNode *sprite;
@property (nonatomic) BOOL isMoving;

-(void)initialize:(NSString *)imageName withPositionX:(float)x withPositionY:(float)y;
-(float)calcDistanceToObject:(float)withObjectPosX withObjectPosY:(float)posY;

@end
